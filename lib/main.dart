import 'package:shun_wei_application/helper/bloc_provider_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app/routes/app_routes.dart';
import 'app/routes/route_generator.dart';
import 'app/themes/build_material_color.dart';
import 'app/themes/color_app.dart';
@pragma('vm:entry-point')
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  EasyLocalization.logger.enableBuildModes = [];
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  // await FirebaseApi().initsNotifications();
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('km'), Locale('en')],
      path: 'assets/translations', 
      startLocale: const Locale('km'),
      child: const MyApp(),
    ),
  );
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final fontName = context.locale.toString();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      builder: (context, child) {
        final mediaQueryData = MediaQuery.of(context);
        final scale = mediaQueryData.textScaleFactor.clamp(1.0, 1.2);
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
          child: child!,
        );
      },
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      initialRoute: Routes.CREATEWARHOUSE,
      onGenerateRoute: RouteGenerator.generateRoute,

      theme: ThemeData(
        primarySwatch: buildMaterialColor(Colorconstand.primaryColor),
        fontFamily: fontName == "en" ? "KantumruyPro" : "KantumruyPro",
      ),
    );
    // return MultiBlocProvider(
    //   providers: listBlocProvider,
    //   child: MaterialApp(
    //     builder: (context, child) {
    //       final mediaQueryData = MediaQuery.of(context);
    //       final scale = mediaQueryData.textScaleFactor.clamp(1.0, 1.2);
    //       return MediaQuery(
    //         data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
    //         child: child!,
    //       );
    //     },
    //     debugShowCheckedModeBanner: false,
    //     localizationsDelegates: context.localizationDelegates,
    //     supportedLocales: context.supportedLocales,
    //     locale: context.locale,
    //     initialRoute: Routes.SPLAHSSCREEN,
    //     onGenerateRoute: RouteGenerator.generateRoute,

    //     theme: ThemeData(
    //       primarySwatch: buildMaterialColor(Colorconstands.primaryColor),
    //       fontFamily: fontName == "en" ? "KantumruyPro" : "KantumruyPro",
    //     ),
    //   ),
    // );
  }
}
