import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../../storages/get_storage.dart';
import '../../storages/save_storage.dart';
class FirebaseApi{
  final GetStoragePref _getStoragePref = GetStoragePref();
  SaveStoragePref _saveStorage = SaveStoragePref();
  final _firebaseMessaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  void handleMessage(RemoteMessage? message,){
  }
  Future<void> firebaseInit() async{
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      sound: true,
      badge: true,
      alert: true,
    );
    FirebaseMessaging.instance.getInitialMessage().then(handleMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(handleMessage);
    FirebaseMessaging.onMessage.listen((RemoteMessage? message) {
      debugPrint("title : ${message!.notification?.title}");
      debugPrint("body : ${message.notification?.body}");
      debugPrint("payload : ${message.data}");
      debugPrint("payload : ${message.data["type"]}");
      String payloadData = jsonEncode(message.data);
      debugPrint("payload : $payloadData");
      if(defaultTargetPlatform == TargetPlatform.android){
        showSimpleNotification(
          title: message.notification!.title!,
          body: message.notification!.body!,
          payload: payloadData
        );
      }
      else{
         showSimpleNotification(
          title: message.notification!.title!,
          body: message.notification!.body!,
          payload: payloadData
        );
      }
    });
  }
  // Future<void> initsNotifications() async{
  //   var token = await _getStoragePref.getToken();
  //   await _firebaseMessaging.requestPermission(
  //     alert: true,
  //     badge: true,
  //     sound: true,
  //   );
  //     if(token =="" || token==null){
  //       if (defaultTargetPlatform == TargetPlatform.android){
  //           var tokenDevice = await _firebaseMessaging.getToken();   
  //           debugPrint("$tokenDevice");
           
  //       }else{
  //           var tokenDevice = await _firebaseMessaging.getToken();   
  //           debugPrint("$tokenDevice");
  //       }
  //     }
  //     else{
  //     }
  //   firebaseInit();
  // }
  Future showSimpleNotification({
    required String title,
    required String body,
    required String payload,
  }) async {
    const DarwinNotificationDetails darwinNotificationDetails = DarwinNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('your channel id', 'your channel name',
            channelDescription: 'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');
    const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails,iOS: darwinNotificationDetails);
    await _flutterLocalNotificationsPlugin.show(0, title, body, notificationDetails, payload: payload);
  }


}