abstract class Routes {
  Routes._();

  static const SPLAHSSCREEN = _Paths.SPLASH;
  static const HOMESCREEN = _Paths.HOME;
  static const CREATEWARHOUSE = _Paths.CREATEWAR;

}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const HOME = '/home';
  static const CREATEWAR = '/create_warhouse';

}
