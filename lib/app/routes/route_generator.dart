import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/page/create_warhosue/create_warhouse_screen.dart';
import 'package:shun_wei_application/app/page/home_page/home_page_admin.dart';
import '../page/splash_page/splash_page.dart';
import 'app_routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.HOMESCREEN:
        return MaterialPageRoute(builder: (_) => const HomePageAdmin());
      case Routes.SPLAHSSCREEN:
        return MaterialPageRoute(builder: (_) => const SplashPage());
      case Routes.CREATEWARHOUSE:
        return MaterialPageRoute(builder: (_) => const CreatWarhouseScreen());  
      default:
         return MaterialPageRoute(builder: (_) => const HomePageAdmin());
    }
  }
}
