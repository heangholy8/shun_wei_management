import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shun_wei_application/app/page/notification_page/notification_page.dart';
import 'package:shun_wei_application/app/resources/asset_resource.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/themes/themes.dart';
import 'package:shun_wei_application/app/widget/button_widget/button_custom.dart';
import 'package:shun_wei_application/app/widget/card_widget/warehouse_card.dart';

import '../detail_page/warehouse_detail_page.dart';


class HomePageSuperAdmin extends StatefulWidget {
  const HomePageSuperAdmin({Key? key}) : super(key: key);

  @override
  State<HomePageSuperAdmin> createState() => _HomePageSuperAdminState();
}

class _HomePageSuperAdminState extends State<HomePageSuperAdmin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Container(
                            margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 12),
                            child: Hero(tag: "Herologo",
                              child: Image.asset("assets/logo/logo-shun-wei.png",width: 65,)
                            ),
                          ),
                          Container(
                            child: Text("SHUN WEI FANG \nZHI KE JI",style: ThemeStyle.font_20_Semibold.copyWith(color: Colorconstand.netureWhite),textAlign: TextAlign.left,),
                          ),
                          
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const  NotificationPage()),
                        );
                      },
                      child: Container(
                        margin:const EdgeInsets.symmetric(horizontal: 18),
                        height: 32,width: 32,
                        child: Stack(
                          children: [
                            Center(
                              child: SvgPicture.asset(ImageAssets.icon_notification,width: 28,),
                            ),
                            Positioned(
                              top: 0,right: 0,
                              child: Container(
                                height: 12,width: 12,
                                decoration:const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colorconstand.netureRed
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 35,),
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:const BoxDecoration(
                    color: Colorconstand.netureWhite,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))
                  ),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:Row(
                          children: [
                            const Expanded(child: Text("Manage Warehouses",style: ThemeStyle.font_20_Semibold,)),
                            Container(
                              alignment: Alignment.centerRight,
                              child: ButtonCustom(
                                onPressed: (){},
                                buttonColor: Colorconstand.primaryColor, 
                                radius: 8, 
                                buttonPadingHor: 8, 
                                buttonPadingVer: 4, 
                                iconLeft: true, 
                                iconCenter: true, 
                                iconRight: false, 
                                disbleIcons: false, 
                                buttonTitle: "Create warehouse", 
                                styleButtonTitle: ThemeStyle.font_14_Semibold.copyWith(color: Colorconstand.netureWhite), 
                                iconsChild:const Icon(Icons.add_outlined,size: 16,color: Colorconstand.netureWhite,), 
                                borderButtonColor: Colors.transparent
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: GridView.builder(
                          padding:const EdgeInsets.only(top: 8,bottom: 28,left: 18,right: 18),
                          scrollDirection: Axis.vertical,
                          physics:const BouncingScrollPhysics(),
                          itemCount:5,
                          gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 1,
                              crossAxisCount: 2,
                              crossAxisSpacing: 18.0,
                              mainAxisSpacing: 18.0),
                          itemBuilder: (BuildContext context, int index) {
                            return WareHouseCard(
                              totalStockIn: "30 Ton", 
                              totalStockOut: "12 Ton", 
                              discription: "G Proy Tep Sakada", 
                              title: "Warehouses ${index+1}", 
                              onPressed: (){
                                 Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => WareHouseDetailPage(warehouseName: "Warehouses ${index+1}",)),
                                    );
                              }, 
                              radius: 16, 
                              backgroundColorCard: Colorconstand.netureWhite
                            );
                          }
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}