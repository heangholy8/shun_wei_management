import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_admin_page.dart';
import 'package:shun_wei_application/app/page/stock_page/stock_in_admin_page.dart';
import 'package:shun_wei_application/app/resources/asset_resource.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/themes/themes.dart';
import 'package:shun_wei_application/app/widget/card_widget/discover_feature_card.dart';

import '../notification_page/notification_page.dart';
import '../stock_page/stock_out_admin_page.dart';

class HomePageAdmin extends StatefulWidget {
  const HomePageAdmin({Key? key}) : super(key: key);

  @override
  State<HomePageAdmin> createState() => _HomePageAdminState();
}

class _HomePageAdminState extends State<HomePageAdmin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Container(
                            margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 12),
                            child: Hero(tag: "Herologo",
                              child: Image.asset("assets/logo/logo-shun-wei.png",width: 65,)
                            ),
                          ),
                          Container(
                            child: Text("SHUN WEI FANG \nZHI KE JI",style: ThemeStyle.font_20_Semibold.copyWith(color: Colorconstand.netureWhite),textAlign: TextAlign.left,),
                          ),
                          
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const  NotificationPage()),
                        );
                      },
                      child: Container(
                        margin:const EdgeInsets.symmetric(horizontal: 18),
                        height: 32,width: 32,
                        child: Stack(
                          children: [
                            Center(
                              child: SvgPicture.asset(ImageAssets.icon_notification,width: 28,),
                            ),
                            Positioned(
                              top: 0,right: 0,
                              child: Container(
                                height: 12,width: 12,
                                decoration:const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colorconstand.netureRed
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration:const BoxDecoration(
                    color: Colorconstand.netureWhite,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                          child:const Text("Discover Features",style: ThemeStyle.font_20_Semibold,),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 18),
                          child: Row(
                            children: [
                              Expanded(
                                child: DiscoverFeatureCard(
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const StockInAdminPage()),
                                    );
                                  },
                                  backgroundColorCard: Colorconstand.netureWhite, 
                                  discriptionColor: Colorconstand.black,
                                  titleColor: Colorconstand.black, 
                                  discription: "Today Quantity", 
                                  type: 1, 
                                  total: "28 In", 
                                  title: "Stock in", 
                                  radius: 12,
                                  totalStockAling: Alignment.centerLeft,
                                ),
                              ),
                              const SizedBox(width: 22,),
                              Expanded(
                                child: DiscoverFeatureCard(
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const StockOutAdminPage()),
                                    );
                                  },
                                  backgroundColorCard: Colorconstand.netureWhite, 
                                  discriptionColor: Colorconstand.black,
                                  titleColor: Colorconstand.black, 
                                  discription: "", 
                                  type: 2, 
                                  total: "30 Out", 
                                  title: "Stock out", 
                                  radius: 12,
                                  totalStockAling: Alignment.centerRight,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 20,),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 18),
                          child: Row(
                            children: [
                              Expanded(
                                child: DiscoverFeatureCard(
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const HistoryAdminPage()),
                                    );
                                  },
                                  backgroundColorCard: Colorconstand.netureWhite, 
                                  discriptionColor: Colorconstand.black,
                                  titleColor: Colorconstand.black, 
                                  discription: "Discription", 
                                  type: 3, 
                                  total: "28 In", 
                                  title: "History", 
                                  radius: 12,
                                  totalStockAling: Alignment.centerLeft,
                                ),
                              ),
                              const SizedBox(width: 22,),
                              Expanded(
                                child: Container(),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}