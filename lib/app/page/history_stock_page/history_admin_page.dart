import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_in_page.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_out_page.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/header_widget/header_icon_left.dart';

import '../../themes/themes.dart';
import '../../widget/card_widget/discover_feature_card.dart';

class HistoryAdminPage extends StatefulWidget {
  const HistoryAdminPage({Key? key}) : super(key: key);

  @override
  State<HistoryAdminPage> createState() => _HistoryAdminPageState();
}

class _HistoryAdminPageState extends State<HistoryAdminPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.darkGrey,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: "History"),
              ),
              Expanded(
                child: Container(
                  color: Colorconstand.netureWhite,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Overview of Stock",style: ThemeStyle.font_20_Semibold,),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Explore Detail of Stock",style: ThemeStyle.font_20_Semibold,),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 18),
                        child: Row(
                          children: [
                            Expanded(
                              child: DiscoverFeatureCard(
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const HistoryStockInPage()),
                                  );
                                },
                                backgroundColorCard: Colorconstand.netureWhite, 
                                discriptionColor: Colorconstand.black,
                                titleColor: Colorconstand.black, 
                                discription: "", 
                                type: 1, 
                                total: "", 
                                title: "Stock in", 
                                radius: 12,
                                totalStockAling: Alignment.centerLeft,
                              ),
                            ),
                            const SizedBox(width: 22,),
                            Expanded(
                              child: DiscoverFeatureCard(
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const HistoryStockOutPage()),
                                  );
                                },
                                backgroundColorCard: Colorconstand.netureWhite, 
                                discriptionColor: Colorconstand.black,
                                titleColor: Colorconstand.black, 
                                discription: "", 
                                type: 2, 
                                total: "", 
                                title: "Stock out", 
                                radius: 12,
                                totalStockAling: Alignment.centerRight,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}