import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/header_widget/header_icon_left.dart';

import '../../themes/themes.dart';

class HistoryStockOutPage extends StatefulWidget {
  const HistoryStockOutPage({Key? key}) : super(key: key);

  @override
  State<HistoryStockOutPage> createState() => _HistoryStockOutPageState();
}

class _HistoryStockOutPageState extends State<HistoryStockOutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.darkGrey,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: "Stock Out"),
              ),
              Expanded(
                child: Container(
                  color: Colorconstand.netureWhite,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Month",style: ThemeStyle.font_18_Varible,),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Stock Out Detail",style: ThemeStyle.font_18_Varible,),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Date: 01 Jan 2024 (Today)",style: ThemeStyle.font_18_Varible,),
                      ),
                      Container(
                        height: 55,
                        padding:const EdgeInsets.symmetric(horizontal: 12),
                        decoration: const BoxDecoration(
                          color: Colorconstand.netureWhite,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Color.fromARGB(255, 236, 230, 230),
                                blurRadius: 15.0,
                                offset: Offset(1.0, 1.75)
                            )
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const[
                            Expanded(child: Text("No.",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.left,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Weight (Kg)",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Type",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Date",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding:const EdgeInsets.symmetric(horizontal: 12),
                          child: ListView.builder(
                            padding:const EdgeInsets.all(0),
                            itemCount: 5,
                            itemBuilder: (context, index) {
                              return Container(
                                margin:const EdgeInsets.only(top: 16),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children:[
                                    Expanded(child: Text("${index+1}.",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.left,)),
                                    const SizedBox(width: 8,),
                                    const Expanded(child: Text("100",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                    const SizedBox(width: 8,),
                                    Expanded(child: Text("Type ${index+1}",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                    const SizedBox(width: 8,),
                                    const Expanded(child: Text("01 Jan 2024",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}