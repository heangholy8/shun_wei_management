import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';

import '../../themes/themes.dart';
import '../../widget/button_widget/button_custom.dart';
import '../../widget/header_widget/header_icon_left.dart';
import '../../widget/textFild_widget/textFormFild_custom.dart';

class CreatWarhouseScreen extends StatefulWidget {
  const CreatWarhouseScreen({Key? key}) : super(key: key);

  @override
  State<CreatWarhouseScreen> createState() => _CreatWarhouseScreenState();
}

class _CreatWarhouseScreenState extends State<CreatWarhouseScreen> {
  TextEditingController warehouse = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom:false,
        child: Container(child: Column(
          children: [
            Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: "Create Warehouse"),
              ),
            Expanded(child: Container(
              padding: const EdgeInsets.symmetric(vertical: 28,horizontal: 10),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                   const Text("Please enter any information about warehouse",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),),
                   const SizedBox(height: 30,),
                   const Text("Warehouse Name",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700)),
                   const SizedBox(height: 18,),
                   Container(
                    child: TextFormFildCustom(
                      onChanged:(String value) {
                        setState(() {
                          
                        });
                      },
                      hintext: "Warehouse Name", 
                      obscureText: false,
                      hintStyle: ThemeStyle.font_14_Rigular, 
                      controller: warehouse, 
                      radius: 12, 
                      borderColor: Colorconstand.grey, 
                      panddingVer: 16, 
                      panddingHor: 12, 
                      keyboardType: TextInputType.text
                    ),
                  ),
                  const SizedBox(height: 25,),
                  const Text("Assistance",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700)),
                  const SizedBox(height: 18,),
                  GestureDetector(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colorconstand.grey,),
                        borderRadius: BorderRadius.circular(12)
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          Expanded(
                            child: GridView.builder(
                                shrinkWrap: true,
                                padding:const EdgeInsets.all(0),
                                itemCount: 2,
                                gridDelegate:const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: (2),childAspectRatio: 3.5,mainAxisSpacing: 15,crossAxisSpacing: 0.1),
                                itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  alignment: Alignment.centerLeft,
                                  child: ButtonCustom(
                                    onPressed: (){},
                                    buttonColor: Colorconstand.sconary.withOpacity(0.2), 
                                    radius: 28, 
                                    buttonPadingHor: 10, 
                                    buttonPadingVer: 8, 
                                    iconLeft: false, 
                                    iconCenter: true, 
                                    iconRight: true, 
                                    disbleIcons: false, 
                                    buttonTitle: "Nico La Tesla", 
                                    styleButtonTitle: ThemeStyle.font_14_Semibold.copyWith(color: Colorconstand.black), 
                                    iconsChild:const Padding(
                                      padding:  EdgeInsets.only(left: 6),
                                      child: Icon(Icons.cancel_outlined,size: 20,color: Colorconstand.black,),
                                    ), 
                                    borderButtonColor: Colors.transparent
                                  ),
                                );
                              }),
                          ), 
                          Container(
                            margin:const EdgeInsets.only(left: 12),
                            child:const Icon(Icons.keyboard_arrow_down_rounded,size: 30,color: Colorconstand.primaryColor,)
                          ),
                        ],
                      ),
                    ),
                  ),
                const SizedBox(height: 25,),
                const Text("Accessibility",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700)),
                const SizedBox(height: 18,),
                Expanded(
                  child: ListView.builder(
                    padding: const EdgeInsets.only(left: 18),
                    shrinkWrap: true,
                    itemCount: 2,
                    itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 18),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text("Nico La Tesla",style: ThemeStyle.font_18_Rigular,),
                            Row(
                              children: [
                                StockInOutWidget(isOut: false,),
                                const SizedBox(width: 20,),
                                StockInOutWidget(isOut: true,),
                              ],
                            ),
                        ],
                      ),
                    );
                  },),
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: ButtonCustom(
                    onPressed: (){},
                    buttonColor: Colorconstand.sconary, 
                    radius: 8, 
                    buttonPadingHor: 10, 
                    buttonPadingVer: 8, 
                    iconLeft: false, 
                    iconCenter: true, 
                    iconRight: true, 
                    disbleIcons: true, 
                    buttonTitle: "Done", 
                    styleButtonTitle: ThemeStyle.font_20_Semibold.copyWith(color: Colorconstand.netureWhite), 
                    iconsChild:Icon(Icons.cancel_outlined,size: 20,color: Colorconstand.black,), 
                    borderButtonColor: Colors.transparent
                  ),
                ), 
              ]),
            )),
          ],
        ),)),
    );
  }
}

class StockInOutWidget extends StatelessWidget {
  bool isOut;
  VoidCallback? onPressed;
  StockInOutWidget({
    Key? key,
    required this.isOut,
    this.onPressed
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      IconButton(onPressed: onPressed, icon: const Icon(Icons.check_box_outline_blank,size: 26,color: Colorconstand.grey,)),
      Text(isOut?"Stock Out":"Stock In",style: ThemeStyle.font_18_Rigular)
    ],);
  }
}