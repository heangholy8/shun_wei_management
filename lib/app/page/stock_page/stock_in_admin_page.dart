import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/header_widget/header_icon_left.dart';

import '../../themes/themes.dart';

class StockInAdminPage extends StatefulWidget {
  const StockInAdminPage({Key? key}) : super(key: key);

  @override
  State<StockInAdminPage> createState() => _StockInAdminPageState();
}

class _StockInAdminPageState extends State<StockInAdminPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: "Stock In"),
              ),
              Expanded(
                child: Container(
                  color: Colorconstand.netureWhite,
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 18,horizontal: 20),
                        child: Row(
                          children: const[
                            Expanded(child: Text("Product In",style: ThemeStyle.font_18_Rigular,textAlign: TextAlign.left,)),
                            SizedBox(width: 12,),
                            Expanded(child: Text("Total:30",style: ThemeStyle.font_20_Semibold,textAlign: TextAlign.right,)),
                          ],
                        ),
                      ),
                      Container(
                        height: 55,
                        padding:const EdgeInsets.symmetric(horizontal: 12),
                        decoration: const BoxDecoration(
                          color: Colorconstand.netureWhite,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Color.fromARGB(255, 236, 230, 230),
                                blurRadius: 15.0,
                                offset: Offset(1.0, 1.75)
                            )
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const[
                            Expanded(child: Text("No.",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.left,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Weight (Kg)",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Type",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                            SizedBox(width: 8,),
                            Expanded(child: Text("Date",style: ThemeStyle.font_16_Semibold,textAlign: TextAlign.center,)),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding:const EdgeInsets.symmetric(horizontal: 12),
                          child: ListView.builder(
                            padding:const EdgeInsets.all(0),
                            itemCount: 5,
                            itemBuilder: (context, index) {
                              return Container(
                                margin:const EdgeInsets.only(top: 16),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children:[
                                    Expanded(child: Text("${index+1}.",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.left,)),
                                    const SizedBox(width: 8,),
                                    const Expanded(child: Text("100",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                    const SizedBox(width: 8,),
                                    Expanded(child: Text("Type ${index+1}",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                    const SizedBox(width: 8,),
                                    const Expanded(child: Text("01 Jan 2024",style: ThemeStyle.font_16_Rigular,textAlign: TextAlign.center,)),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}