import 'package:shun_wei_application/app/resources/asset_resource.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/themes.dart';
import '../role_login_page/role_login_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1300), () async{
      Navigator.pushReplacement(context,PageRouteBuilder(pageBuilder: (context, animation1, animation2) => const RoleLoginPage(),
                        transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.netureWhite,
      body: SafeArea(
        bottom: false,
        child: Padding(
          padding:const EdgeInsets.symmetric(horizontal: 22,vertical: 28),
          child: Column(
            children: [
              const Expanded(child: SizedBox()),
              Container(
                padding:const EdgeInsets.all(28),
                decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colorconstand.primaryColor
                ),
                child: Hero(tag: "Herologo",child: Image.asset(ImageAssets.LogoShunWei,width: 145,height: 145,)),
              ),
              const SizedBox(height: 8,),
              Center(
                child: Text("SHUN WEI FANG \nZHI KE JI",style: ThemeStyle.font_24_Semibold.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
              ),
              const Expanded(child: SizedBox())
            ],
          ),
        ),
      ),
    );
  }
}