import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shun_wei_application/app/page/home_page/home_page_admin.dart';
import 'package:shun_wei_application/app/page/home_page/home_page_super_admin.dart';

import '../../resources/asset_resource.dart';
import '../../themes/color_app.dart';
import '../../themes/themes.dart';
import '../../widget/button_widget/button_custom.dart';
import '../login_page/login_page.dart';

class RoleLoginPage extends StatefulWidget {
  const RoleLoginPage({Key? key}) : super(key: key);

  @override
  State<RoleLoginPage> createState() => _RoleLoginPageState();
}

class _RoleLoginPageState extends State<RoleLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.netureWhite,
      body: Stack(
        children: [
          Positioned(
            top: 0,right: 0,
            child: SvgPicture.asset("assets/images/svg/bacground2_page_role.svg"),
          ),
          Positioned(
            bottom: 0,left: 0,
            child: SvgPicture.asset("assets/images/svg/bacgrount1_page_role.svg"),
          ),
          SafeArea(
            bottom: false,
            child: Padding(
              padding:const EdgeInsets.symmetric(horizontal: 22,vertical: 28),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Expanded(child: SizedBox()),
                          Container(
                            padding:const EdgeInsets.all(28),
                            decoration:const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colorconstand.primaryColor
                            ),
                            child: Hero(tag: "Herologo",child: Image.asset(ImageAssets.LogoShunWei,width: 65,height: 65,)),
                          ),
                          const SizedBox(height: 8,),
                          Center(
                            child: Text("SHUN WEI FANG \nZHI KE JI",style: ThemeStyle.font_24_Semibold.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
                          ),
                          const Expanded(child: SizedBox())
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Please Select Your Role",style: ThemeStyle.font_20_Rigular.copyWith(color: Colorconstand.darkGrey),textAlign: TextAlign.center,),
                        ),
                        const SizedBox(height: 35,),
                        Container(
                          child: Row(
                            children: [
                              Expanded(
                                child: ButtonCustom(
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => LoginPage(role: 1,)),
                                    );
                                  }, 
                                  buttonColor: Colorconstand.primaryColor,
                                  radius: 16, 
                                  buttonPadingHor: 12, 
                                  buttonPadingVer: 24, 
                                  iconLeft: false, 
                                  iconCenter: false, 
                                  iconRight: false, 
                                  disbleIcons: true, 
                                  buttonTitle: "Super admin", 
                                  styleButtonTitle: ThemeStyle.font_22_Semibold.copyWith(color: Colorconstand.netureWhite), 
                                  iconsChild: Container(), 
                                  borderButtonColor: Colors.transparent
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 24,),
                        Container(
                          child: Row(
                            children: [
                              Expanded(
                                child: ButtonCustom(
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => LoginPage(role: 2,)),
                                    );
                                  }, 
                                  buttonColor: Colorconstand.netureWhite,
                                  radius: 16, 
                                  buttonPadingHor: 12, 
                                  buttonPadingVer: 24, 
                                  iconLeft: false, 
                                  iconCenter: false, 
                                  iconRight: false, 
                                  disbleIcons: true, 
                                  buttonTitle: "Admin", 
                                  styleButtonTitle: ThemeStyle.font_22_Semibold.copyWith(color: Colorconstand.black), 
                                  iconsChild: Container(), 
                                  borderButtonColor: Colors.transparent
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}