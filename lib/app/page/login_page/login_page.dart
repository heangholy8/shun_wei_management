import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/page/home_page/home_page_admin.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/textFild_widget/textFormFild_custom.dart';

import '../../themes/themes.dart';
import '../../widget/button_widget/button_custom.dart';
import '../home_page/home_page_super_admin.dart';

class LoginPage extends StatefulWidget {
  int role ;//1= super admin  2= admin
  LoginPage({Key? key,required this.role}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userController = TextEditingController();
  TextEditingController passController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.netureWhite,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: SingleChildScrollView(
            child: Container(
              margin:const EdgeInsets.symmetric(horizontal: 22),
              child: Column(
                children: [
                  const SizedBox(height: 65,),
                  Container(
                    alignment: Alignment.center,
                    child: Text("Welcome \n${widget.role == 1?"Super Admin":"Admin"}",style: ThemeStyle.font_32_Semibold.copyWith(color: Colorconstand.primaryColor),textAlign: TextAlign.center,),
                  ),
                  const SizedBox(height: 25,),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Login to Your Account",style: ThemeStyle.font_16_Rigular.copyWith(color: Colorconstand.black),textAlign: TextAlign.left,),
                  ),
                  const SizedBox(height: 15,),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Please enter your current User Code & Pass Code",style: ThemeStyle.font_14_Rigular.copyWith(color: Colorconstand.darkGrey),textAlign: TextAlign.left,),
                  ),
                  const SizedBox(height: 25,),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("User Code",style: ThemeStyle.font_16_Semibold.copyWith(color: Colorconstand.black),textAlign: TextAlign.center,),
                  ),
                  const SizedBox(height: 15,),
                  Container(
                    child: TextFormFildCustom(
                      onChanged:(String value) {
                        setState(() {
                          
                        });
                      },
                      hintext: "User Code", 
                      obscureText: false,
                      hintStyle: ThemeStyle.font_14_Rigular, 
                      controller: userController, 
                      radius: 12, 
                      borderColor: Colorconstand.grey, 
                      panddingVer: 16, 
                      panddingHor: 12, 
                      keyboardType: TextInputType.text
                    ),
                  ),
                  const SizedBox(height: 25,),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Pass Code",style: ThemeStyle.font_16_Semibold.copyWith(color: Colorconstand.black),textAlign: TextAlign.center,),
                  ),
                  const SizedBox(height: 15,),
                  Container(
                    child: TextFormFildCustom(
                      onChanged:(String value) {
                        setState(() {
                          
                        });
                      },
                      obscureText: true,
                      hintext: "Pass Code", 
                      hintStyle: ThemeStyle.font_14_Rigular, 
                      controller: passController, 
                      radius: 12, 
                      borderColor: Colorconstand.grey, 
                      panddingVer: 16, 
                      panddingHor: 12, 
                      keyboardType: TextInputType.text
                    ),
                  ),
                  const SizedBox(height: 35,),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                          child: ButtonCustom(
                            onPressed: userController.text == "" || passController.text == "" ? null:(){
                              if(widget.role == 1){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => const HomePageSuperAdmin()),
                                );
                              }
                              else{
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => const HomePageAdmin()),
                                );
                              }
                              
                            }, 
                            buttonColor: Colorconstand.primaryColor,
                            radius: 12, 
                            buttonPadingHor: 12, 
                            buttonPadingVer: 16, 
                            iconLeft: false, 
                            iconCenter: false, 
                            iconRight: false, 
                            disbleIcons: true, 
                            buttonTitle: "Login", 
                            styleButtonTitle: ThemeStyle.font_22_Semibold.copyWith(color: Colorconstand.netureWhite), 
                            iconsChild: Container(), 
                            borderButtonColor: Colors.transparent
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}