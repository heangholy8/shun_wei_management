import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_in_page.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_out_page.dart';
import 'package:shun_wei_application/app/page/stock_page/stock_in_admin_page.dart';
import 'package:shun_wei_application/app/page/stock_page/stock_out_admin_page.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/header_widget/header_icon_left.dart';

import '../../themes/themes.dart';
import '../../widget/card_widget/discover_feature_card.dart';

class WareHouseDetailPage extends StatefulWidget {
  String warehouseName;
  WareHouseDetailPage({Key? key,required this.warehouseName}) : super(key: key);

  @override
  State<WareHouseDetailPage> createState() => _WareHouseDetailPageState();
}

class _WareHouseDetailPageState extends State<WareHouseDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: widget.warehouseName),
              ),
              Expanded(
                child: Container(
                  color: Colorconstand.netureWhite,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                        child:const Text("Discover Features",style: ThemeStyle.font_20_Semibold,),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 18),
                        child: Row(
                          children: [
                            Expanded(
                              child: DiscoverFeatureCard(
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const StockInAdminPage()),
                                  );
                                },
                                backgroundColorCard: Colorconstand.netureWhite, 
                                discriptionColor: Colorconstand.black,
                                titleColor: Colorconstand.black, 
                                discription: "Today Quantity", 
                                type: 1, 
                                total: "30 Ton", 
                                title: "Stock in", 
                                radius: 12,
                                totalStockAling: Alignment.centerLeft,
                              ),
                            ),
                            const SizedBox(width: 22,),
                            Expanded(
                              child: DiscoverFeatureCard(
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const StockOutAdminPage()),
                                  );
                                },
                                backgroundColorCard: Colorconstand.netureWhite, 
                                discriptionColor: Colorconstand.black,
                                titleColor: Colorconstand.black, 
                                discription: "", 
                                type: 2, 
                                total: "10 Ton", 
                                title: "Stock out", 
                                radius: 12,
                                totalStockAling: Alignment.centerRight,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}