import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_in_page.dart';
import 'package:shun_wei_application/app/page/history_stock_page/history_stock_out_page.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/widget/card_widget/notification_card.dart';
import 'package:shun_wei_application/app/widget/header_widget/header_icon_left.dart';

import '../../themes/themes.dart';
import '../../widget/card_widget/discover_feature_card.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key,}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstand.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 12,vertical: 12),
                child: HeaderIconLeft(title: "Notification"),
              ),
              Expanded(
                child: Container(
                  color: Colorconstand.netureWhite,
                  child: ListView.builder(
                    padding:const EdgeInsets.all(0),
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin:const EdgeInsets.symmetric(horizontal: 22,vertical: 16),
                            child:Text("Notification ${index+1}",style: ThemeStyle.font_18_Semibold.copyWith(color: Colorconstand.black),),
                          ),
                          Container(
                            margin:const EdgeInsets.symmetric(horizontal: 18),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: const ScrollPhysics(),
                              padding:const EdgeInsets.all(0),
                              itemCount: 2,
                              itemBuilder: (context, subindex) {
                                return Column(
                                  children: [
                                    Container(
                                      margin:const EdgeInsets.only(bottom: 11,top: 11),
                                      child: NotificationCard(
                                        radius: 12, 
                                        type: subindex+1, 
                                        cardColor: Colorconstand.netureWhite, 
                                        borderColor: Colorconstand.grey, 
                                        timeNotification: "${index+3}:39 AM", 
                                        title: subindex == 0?"Stock In":"Stock Out", 
                                        weight: "${subindex+13} Kg", 
                                        typeName: "Type ${subindex+1}", 
                                        colorTitle: subindex == 0?Colorconstand.primaryColor:Colorconstand.netureRed
                                      ),
                                    ),
                                  ],
                                );
                              }
                            ),
                          )
                        ],
                      );
                    }
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}