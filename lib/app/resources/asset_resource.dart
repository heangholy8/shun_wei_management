// ignore_for_file: constant_identifier_names
const String LOGO_PATH = "assets/logo";
const String SVG_PATH_IMAGE = "assets/images/svg";
const String PNG_PATH_IMAGE = "assets/images/png";
const String ICONS_SVG_PATH = "assets/icons/svg";
const String ICONS_PNG_PATH = "assets/icons/png";

class ImageAssets {

   // <<<<<<<<<<<<<<< LOGO_PATH >>>>>>>>>>>>>>>>> //

   static const String LogoShunWei = 'assets/logo/logo-shun-wei.png';

  // *************** ICONS_SVG_PATH ***************** //

  static const String icon_notification = 'assets/icons/svg/icon-notofication.svg';

// *************** ICONS_PNG_PATH ***************** //

  // static const String notification_outline_icon = '$ICONS_SVG_PATH/vuesax_outline_notification.svg';
 
 // *************** SVG_PATH_IMAGE ***************** //

  // static const String user_edit = '$ICONS_SVG_PATH/vuesax_outline_user-edit.svg';

  // *************** PNG_PATH_IMAGE ***************** //
}
