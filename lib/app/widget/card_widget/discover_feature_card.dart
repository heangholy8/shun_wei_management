import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/themes/themes.dart';

class DiscoverFeatureCard extends StatelessWidget {
  Color backgroundColorCard;
  Color titleColor;
  Color discriptionColor;
  int type; //  1. stock in 2.stock out 3 history
  String title;
  String discription;
  String total;
  double radius;
  VoidCallback onPressed;
  Alignment totalStockAling;
  DiscoverFeatureCard({Key? key,
    required this.backgroundColorCard,
    required this.discriptionColor,
    required this.titleColor,
    required this.discription,
    required this.type,
    required this.total,
    required this.title,
    required this.radius,
    required this.onPressed,
    required this.totalStockAling,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (MediaQuery.of(context).size.width/2) - 48,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        boxShadow:const <BoxShadow>[
          BoxShadow(
              color: Color.fromARGB(255, 236, 230, 230),
              blurRadius: 15.0,
              offset: Offset(1.0, 1.75)
          )
        ],
      ),
      child: MaterialButton(
        elevation: 0,
        color: backgroundColorCard,
        padding:const EdgeInsets.all(0),
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius), // <-- Radius
        ),
        child: Container(
          margin:const EdgeInsets.symmetric(horizontal: 16,vertical: 18),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  child: Row(
                    children: [
                      Container(
                        width: 45,
                        height: 45,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: type == 1?Colorconstand.primaryColor:type == 2?Colorconstand.subRed:Colorconstand.darkGrey
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin:const EdgeInsets.symmetric(horizontal: 12),
                          child: Text(title,style: ThemeStyle.font_18_Semibold.copyWith(color: titleColor),),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text(discription,style: ThemeStyle.font_15_Varible.copyWith(color: discriptionColor),textAlign: TextAlign.left,),
                ),
              ),
              Expanded(
                child: type == 3?Container():Container(
                  child: Row(
                    children: [
                      Container(
                        width: 5,
                        decoration: BoxDecoration(
                          color: type == 1?Colorconstand.primaryColor:Colorconstand.subRed,
                          borderRadius:const BorderRadius.all(Radius.circular(5))
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin:const EdgeInsets.only(left: 12),
                          alignment: totalStockAling,
                          child: Text(total,style: ThemeStyle.font_20_Semibold.copyWith(color: titleColor),),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}