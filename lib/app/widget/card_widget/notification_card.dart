import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';

import '../../themes/themes.dart';

class NotificationCard extends StatelessWidget {
  double radius;
  Color borderColor;
  Color cardColor;
  Color colorTitle;
  String timeNotification;
  String weight;
  String typeName;
  String title;
  int type; // 1 = stock in  2 = stock out
  NotificationCard({Key? key,
  required this.radius,
  required this.type,
  required this.cardColor,
  required this.borderColor,
  required this.timeNotification,
  required this.title,
  required this.weight,
  required this.typeName,
  required this.colorTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        border: Border.all(color: borderColor),
        color: cardColor,
      ),
      child: Container(
        margin:const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
        child: Row(
          children: [
            Expanded(
              child: Row(
                children: [
                  Container(
                    height: 62,width: 62,
                    decoration: BoxDecoration(
                      borderRadius:const BorderRadius.all(Radius.circular(8)),
                      border: Border.all(color: borderColor),
                      color: type == 1?Colorconstand.primaryColor.withOpacity(0.4):Colorconstand.netureRed.withOpacity(0.4)
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.symmetric(vertical: 6,horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text(title,style: ThemeStyle.font_16_Semibold.copyWith(color: colorTitle),textAlign: TextAlign.left,),
                        ),
                        const SizedBox(height: 6,),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Weight: $weight",style: ThemeStyle.font_14_Varible.copyWith(color: Colorconstand.black),textAlign: TextAlign.left,),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Type: $typeName",style: ThemeStyle.font_14_Varible.copyWith(color: Colorconstand.black),textAlign: TextAlign.left,),
                        )
                      ],
                    ),
                  )
                  
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Text(timeNotification,style: ThemeStyle.font_16_Varible.copyWith(color: Colorconstand.black),textAlign: TextAlign.right,),
            )
          ],
        ),
      ),
    );
  }
}