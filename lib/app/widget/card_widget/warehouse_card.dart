import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';
import 'package:shun_wei_application/app/themes/themes.dart';

class WareHouseCard extends StatelessWidget {
  String title;
  Color backgroundColorCard;
  double radius;
  String discription;
  String totalStockIn;
  String totalStockOut;
  VoidCallback onPressed;
  WareHouseCard({Key? key,
    required this.totalStockIn,
    required this.totalStockOut,
    required this.discription,
    required this.title,
    required this.onPressed,
    required this.radius,
    required this.backgroundColorCard,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        boxShadow:const <BoxShadow>[
          BoxShadow(
              color: Color.fromARGB(255, 236, 230, 230),
              blurRadius: 15.0,
              offset: Offset(1.0, 1.75)
          )
        ],
      ),
      child: MaterialButton(
        elevation: 0,
        color: backgroundColorCard,
        padding:const EdgeInsets.all(0),
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius), // <-- Radius
        ),
        child: Container(
          margin:const EdgeInsets.symmetric(horizontal: 16,vertical: 24),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(title,style: ThemeStyle.font_20_Semibold.copyWith(color: Colorconstand.primaryColor),maxLines: 2,overflow: TextOverflow.ellipsis,)
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(discription,style: ThemeStyle.font_15_Varible.copyWith(color: Colorconstand.darkGrey),maxLines: 2,overflow: TextOverflow.ellipsis,)
                      ),
                      
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Stock in:",style: ThemeStyle.font_14_Varible.copyWith(color: Colorconstand.netureRed),),
                            Expanded(child: Text(totalStockIn,style: ThemeStyle.font_16_Semibold.copyWith(color: Colorconstand.black),textAlign: TextAlign.end,maxLines: 2,overflow: TextOverflow.ellipsis,)),
                          ],
                        )
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Stock out:",style: ThemeStyle.font_14_Varible.copyWith(color: Colorconstand.black),),
                            Expanded(child: Text(totalStockOut,style: ThemeStyle.font_16_Semibold.copyWith(color: Colorconstand.black),textAlign: TextAlign.end,maxLines: 2,overflow: TextOverflow.ellipsis,)),
                          ],
                        )
                      ),
                      
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}