import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';

import '../../themes/themes.dart';

class HeaderIconLeft extends StatelessWidget {
  String title;
  HeaderIconLeft({Key? key,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: (){
              Navigator.of(context).pop();
            }, 
            icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,color:Colorconstand.netureWhite)
          ),
          Container(
            margin:const EdgeInsets.symmetric(horizontal: 0),
            child: Text(title,style: ThemeStyle.font_20_Semibold.copyWith(color: Colorconstand.netureWhite),textAlign: TextAlign.left,),
          )
        ],
      ),
    );
  }
}