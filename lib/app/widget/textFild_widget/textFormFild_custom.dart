import 'package:flutter/material.dart';

class TextFormFildCustom extends StatelessWidget {
  String hintext;
  TextStyle hintStyle;
  TextInputType keyboardType;
  TextEditingController controller;
  double radius;
  bool obscureText;
  double panddingVer;
  double panddingHor;
  Color borderColor;
  Function(String) onChanged;
  TextFormFildCustom({Key? key,
  required this.hintext,
  required this.hintStyle,
  required this.controller,
  required this.radius,
  required this.borderColor,
  required this.panddingVer,
  required this.panddingHor,
  required this.keyboardType,
  required this.obscureText,
  required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      keyboardType: keyboardType,
      controller: controller,
      obscureText: obscureText,
      obscuringCharacter: "*",
      decoration: InputDecoration(
        hintText: hintext,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(vertical: panddingVer,horizontal: panddingHor),
        border:OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(radius)),
          borderSide: BorderSide(color: borderColor),
        ),
      )
    );
  }
}