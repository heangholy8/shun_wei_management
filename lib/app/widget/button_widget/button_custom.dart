import 'package:flutter/material.dart';
import 'package:shun_wei_application/app/themes/color_app.dart';

class ButtonCustom extends StatelessWidget {
  VoidCallback? onPressed;
  Color buttonColor;
  Color borderButtonColor;
  double radius;
  double buttonPadingHor;
  double buttonPadingVer;
  bool iconLeft;
  bool iconRight;
  bool iconCenter;
  bool disbleIcons;
  String buttonTitle;
  TextStyle styleButtonTitle;
  Widget iconsChild;
  ButtonCustom({Key? key,
      this.onPressed,
      required this.buttonColor,
      required this.radius,
      required this.buttonPadingHor,
      required this.buttonPadingVer,
      required this.iconLeft,
      required this.iconCenter,
      required this.iconRight,
      required this.disbleIcons,
      required this.buttonTitle,
      required this.styleButtonTitle,
      required this.iconsChild,
      required this.borderButtonColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        boxShadow:const <BoxShadow>[
          BoxShadow(
              color: Color.fromARGB(255, 236, 230, 230),
              blurRadius: 15.0,
              offset: Offset(1.0, 1.75)
          )
        ],
      ),
      child: MaterialButton(
         disabledColor: Colorconstand.grey,
         disabledTextColor: Colorconstand.darkGrey,
          elevation: 0,
          color: buttonColor,
          padding:const EdgeInsets.all(0),
          onPressed: onPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius), // <-- Radius
            side: BorderSide(color: borderButtonColor)
          ),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: buttonPadingHor,vertical: buttonPadingVer),
            child: disbleIcons == true? 
             Container(
                  child: Text(buttonTitle,style: styleButtonTitle,textAlign: TextAlign.center,),
                )
             :Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                iconLeft == true? Container(
                  child: iconsChild,
                ):Container(),
                iconCenter == false?Expanded(child: Container()):Container(),
                Container(
                  child: Text(buttonTitle,style: styleButtonTitle,textAlign: TextAlign.center,),
                ),
                iconCenter == false? Expanded(child: Container()):Container(),
                iconRight == true? Container(
                  child: iconsChild,
                ):Container(),
              ],
            ),
          ),
      ),
    );
  }
}