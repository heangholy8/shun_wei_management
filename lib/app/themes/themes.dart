import 'package:flutter/material.dart';

class ThemeStyle {
  static const String kantumruyPro = 'KantumruyPro';
  static const String kantumruyProSemiBold = 'KantumruyPro-SemiBold';
  static const String kantumruyProRegular = 'KantumruyPro-Regular';

  // ============ font size 8 =================

  static const  font_8_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 8.0
  );
  static const  font_8_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 8.0
  );
  static const  font_8_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 8.0
  );
  
  // ============ font size 10 =================

  static const  font_10_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 10.0
  );
  static const  font_10_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 10.0
  );
  static const  font_10_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 10.0
  );

  // ============ font size 12 =================

  static const  font_12_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
  );
  static const  font_12_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
  );
  static const  font_12_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
  );

  // ============ font size 14 =================

  static const  font_14_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
  );
  static const  font_14_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
  );
  static const  font_14_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
  );

  // ============ font size 15 =================

  static const  font_15_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 15.0
  );
  static const  font_15_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 15.0
  );
  static const  font_15_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 15.0
  );

  // ============ font size 16 =================

  static const  font_16_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
  );
  static const  font_16_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
  );
  static const  font_16_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
  );

  // ============ font size 18 =================

  static const  font_18_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
  );
  static const  font_18_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
  );
  static const  font_18_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
  );

  // ============ font size 20 =================

  static const  font_20_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
  );
  static const  font_20_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
  );
  static const  font_20_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
  );

  // ============ font size 22 =================

  static const  font_22_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 22.0
  );
  static const  font_22_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 22.0
  );
  static const  font_22_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 22.0
  );

  // ============ font size 24 =================

  static const  font_24_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
  );
  static const  font_24_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
  );
  static const  font_24_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
  );

  // ============ font size 26 =================

  static const  font_26_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 26.0
  );
  static const  font_26_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 26.0
  );
  static const  font_26_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 26.0
  );

  // ============ font size 28 =================

  static const  font_28_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 28.0
  );
  static const  font_28_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 28.0
  );
  static const  font_28_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 28.0
  );

  // ============ font size 30 =================

  static const  font_30_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 30.0
  );
  static const  font_30_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 30.0
  );
  static const  font_30_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 30.0
  );

  // ============ font size 31 =================

  static const  font_31_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 31.0
  );
  static const  font_31_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 31.0
  );
  static const  font_31_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 31.0
  );

    // ============ font size 32 =================

  static const  font_32_Varible =  TextStyle(
    fontFamily: kantumruyPro,
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
  );
  static const  font_32_Semibold =  TextStyle(
    fontFamily: kantumruyProSemiBold,
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
  );
  static const  font_32_Rigular =  TextStyle(
    fontFamily: kantumruyProRegular,
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
  );
}
