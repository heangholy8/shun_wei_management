import 'package:shun_wei_application/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SaveStoragePref with keyStoragePref {

  void saveJsonToken({required String authModel}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString(jsonToken, authModel)
          .then((value) => debugPrint("Success to store json in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
  }
  //=================End Save defauld child ==============
}
