import 'package:flutter/services.dart';

List<DeviceOrientation> setOrientation = [
  DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
];
