import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RemoveStorageHelper {
  removeToken(context) async {
    await DefaultCacheManager().emptyCache();
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.remove("jsonToken");
      _pref.remove("tokenDevice");
       print("Token was removed from cache");
    } catch (e) {
      print("Error Remove token" + e.toString());
    }
  }
}
